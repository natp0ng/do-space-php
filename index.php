<?php
require 'vendor/autoload.php';
use Aws\S3\S3Client;
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$client = new S3Client([
        'version' => 'latest',
        'region'  => getenv('DO_SPACES_REGION'),
        'endpoint' => getenv('DO_SPACES_ENDPOINT'),
        'credentials' => [
                'key'    => getenv('DO_SPACES_KEY'),
                'secret' => getenv('DO_SPACES_SECRET'),
            ],
]);

$spaces = $client->listBuckets();
foreach ($spaces['Buckets'] as $space){
    echo $space['Name']."\n";
}

$image = '';

if (isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK) {
  // get details of the uploaded file
  $fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
  $fileName = $_FILES['uploadedFile']['name'];
  $fileSize = $_FILES['uploadedFile']['size'];
  $fileType = $_FILES['uploadedFile']['type'];
  $fileNameCmps = explode(".", $fileName);
  $fileExtension = strtolower(end($fileNameCmps));

  $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

  $allowedfileExtensions = array('jpg', 'gif', 'png');
  if (in_array($fileExtension, $allowedfileExtensions)) {
    $result = $client->putObject([
      'Bucket' => 'shophil-static',
      'Key'    => $newFileName,
      'Body'   => file_get_contents($fileTmpPath),
      // 'ACL'    => 'private'
      'ACL'    => 'public-read',
      'ContentType' => 'image/png'
    ]);

    echo '<pre>'; var_dump($result);

    $image = '<img src="https://shophil-static.sgp1.digitaloceanspaces.com/'.$newFileName.'" />';
  }
}
?>
<?=$image?>

<hr>

<form method="POST" action="index.php" enctype="multipart/form-data">
  <label for="files">Select files:</label>
  <input type="file" id="files" name="uploadedFile" multiple><br><br>
  <input type="submit">
</form>
